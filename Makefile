all: testAutoText doc
	./testAutoText < data/LesForcatsDuMariage-lower.txt

testAutoText: cpp/TestAutoText.cpp cpp/TextProdChar.cpp include/TextProdChar.hpp cpp/LexFreq.cpp include/LexFreq.hpp cpp/Context.cpp include/Context.hpp
	g++ -g -I ./include/ cpp/TestAutoText.cpp cpp/TextProdChar.cpp cpp/LexFreq.cpp cpp/Context.cpp -o testAutoText

clean:
	rm -fr *~ cpp/*~ include/*~

wipe: clean
	rm -fr testAutoText autoTextDoc

doc:
	doxygen Doxyfile >/dev/null
