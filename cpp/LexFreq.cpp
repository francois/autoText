#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <ctype.h>

#include "LexFreq.hpp"

using namespace std;

// ========================================
ostream&
operator << (ostream& os, const LexFreq &freqLex) {
  os << " size = " << freqLex.size << endl;
  for (const auto& [key, value]: freqLex.hist)
    os << key << " = " << value << endl;
  return os;
}

LexFreq::LexFreq () :
  size (0) {
}

void
LexFreq::addChar (const string &c) {
  ++hist [c];
  ++size;
}

string
LexFreq::getChar () const {
  if (!size)
    return "*";
  long r (rand () % size);
  for (const auto& [key, value]: hist) {
    if (r <= value)
      return key;
    r -= value;
  }

  return "*";
}

// ========================================
