#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <ctype.h>

#include "Context.hpp"

using namespace std;

// ========================================
const int Context::prevSize (16);
const string Context::end (".?!");
const string Context::sep (" ,;’-");

int
Context::getPrevSize () {
  return prevSize;
}

bool
Context::validChar (const char &c) {
  if (isalpha (c))
    return true;
  return
    end.find (c) != string::npos ||
    sep.find (c) != string::npos;
}

bool
Context::isEnd (const string &c) {
  return end.find (c) != string::npos;
}

bool
Context::isSep (const string &c) {
  return sep.find (c) != string::npos;
}

void
Context::forward (string current) {
  if (!prevSize)
    return;
  if (current == " " && prev.empty ())
    return;
  if (end.find (current) != string::npos) {
    prev.clear ();
    return;
  }
  prev += current;
  if (prev.size () > prevSize) {
    int count (prevSize);
    for (auto rit (prev.crbegin ()); rit != prev.crend (); ++rit)
      // (ASCII || start UTF) && countdown
      if ((! (*rit & 0b10000000) || (*rit & 0b01000000)) && ! --count) {
	prev = prev.substr (prev.crend () - rit -1);
	break;
      }
  }
}

// ========================================
