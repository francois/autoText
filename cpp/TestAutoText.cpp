/*

  La Part du feu. Les Terreurs du bourgeois Prudence et de son ami Furibus, 1873 par M.-L. Gagneur (1832-1902). 

  https://gallica.bnf.fr/services/engine/search/sru?operation=searchRetrieve&exactSearch=false&collapsing=true&version=1.2&query=(dc.creator%20all%20%22Marie-Louise%20Gagneur%22%20or%20dc.contributor%20all%20%22Marie-Louise%20Gagneur%22%20)%20&suggest=10&keywords=Marie-Louise%20Gagneur

  https://fr.wikisource.org/wiki/La_Part_du_feu?rk=42918;4
*/

#include <iostream>
#include <fstream>
#include <cstdlib>

#include "TextProdChar.hpp"

using namespace std;

void
test () {
  for (string line; getline (cin, line); ) {
    for (string::iterator it (line.begin ());
	 it != line.end ();
	 ++it) {
      unsigned char c = *it;
      if (c & 0b10000000) {
	++it;
	cout << c << *it << "\\";
	continue;
      }
      cout << c << "/";
    }
  }
}

// ========================================
int
main (int argc, char** argv) {
  cerr << endl << "    ** Test TextProdChar" << endl;

  // ifstream in ("in.txt");
  // ofstream out ("out.txt");

  TextProdChar textProdChar;
  srand (0); //(time (0));

  //test ();
  textProdChar.learn (cin);
  // cerr << textProdChar << endl;

  textProdChar.prod (cout, 1000);
  cerr << endl;
  return 0;
}

// ========================================
