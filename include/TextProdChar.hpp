/*!
 * @file TextProdChar.hpp
 * @brief calcul de fréquence et prédiction
 * @author F. Merciol
 * @version 0.1
 * @date 18 / août 2023
 */
#ifndef _TextProdChar_hpp_
#define _TextProdChar_hpp_

#include <iostream>
#include <map>

#include "Context.hpp"
#include "LexFreq.hpp"

using namespace std;

// ========================================
class TextProdChar {
  Context context;
  map<string, LexFreq> prevFreq;
public:
  friend ostream& operator << (ostream& os, const TextProdChar &ia);

  void learn (istream &in);
  void prod (ostream &out, const long &size);
};


// ========================================
#endif // _TextProdChar_hpp_
