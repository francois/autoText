/*!
 * @file LexFreq.hpp
 * @brief calcul de fréquence et prédiction
 * @author F. Merciol
 * @version 0.1
 * @date 18 / août 2023
 */
#ifndef _LexFreq_hpp_
#define _LexFreq_hpp_

#include <iostream>
#include <map>

using namespace std;

// ========================================
class LexFreq {
  long size;
  map<string, long> hist;
public:
  friend ostream& operator << (ostream& os, const LexFreq &freqLex);

  LexFreq ();
  long getSize () const { return hist.size (); }
  void addChar (const string &c);
  string getChar () const;
};

// ========================================
#endif // _LexFreq_hpp_
