/*!
 * @file Context.hpp
 * @brief calcul de fréquence et prédiction
 * @author F. Merciol
 * @version 0.1
 * @date 18 / août 2023
 */
#ifndef _Context_hpp_
#define _Context_hpp_

#include <iostream>
#include <map>

using namespace std;

// ========================================
class Context {
  string prev;
  static const int prevSize;
  static const string end;
  static const string sep;
public:
  static int getPrevSize ();
  static bool validChar (const char &c);
  static bool isEnd (const string &c);
  static bool isSep (const string &c);
  bool empty () { return prev.empty (); }
  void reset () { prev.clear (); }
  string getState () { return prev; }
  void forward (string current);
};

// ========================================
#endif // _Context_hpp_
